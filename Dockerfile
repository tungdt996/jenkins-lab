# Stage 1: Build dependencies
FROM node:16-alpine AS builder

# Thiết lập thư mục làm việc
WORKDIR /app

# Copy file package.json và yarn.lock và cài đặt dependencies
COPY package.json yarn.lock ./

# Cài đặt dependencies
RUN yarn install --frozen-lockfile && yarn cache clean

# Copy toàn bộ source code vào container
COPY . .

# Stage 2: Final Image
FROM node:16-alpine

# Thiết lập thư mục làm việc
WORKDIR /app

# Copy chỉ những tệp cần thiết từ stage 1 sang stage 2
COPY --from=builder /app /app

# Thiết lập biến môi trường
ENV NODE_ENV=production

# Expose cổng mà ứng dụng sẽ lắng nghe
EXPOSE 3000

# Lệnh để chạy ứng dụng
CMD ["yarn", "start"]

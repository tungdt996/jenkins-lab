import styles from "./index.less";

export default function IndexPage() {
  return (
    <div>
      <h1 className={styles.title}>Welcome to My Page React sample CICD by Tungdt v23.0.0</h1>
    </div>
  );
}
